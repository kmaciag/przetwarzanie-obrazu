#include <libgimp/gimp.h>
#include <libgimp/gimpui.h>
#include <assert.h>

#define median(a,n) kth_smallest(a,n,(((n)&1)?((n)/2):(((n)/2)-1)))
#define ELEM_SWAP(a,b) { register guchar t=(a);(a)=(b);(b)=t; }
typedef struct
{
  gchar radius;
} MyBlurVals;

static void query(void);

static void run(const gchar *name, gint nparams, const GimpParam *param, gint *nreturn_vals, GimpParam **return_vals);

static void blur(GimpDrawable     *drawable);

static void init_mem(guchar **row, guchar **outrow, gint num_bytes);

static void process_rect(guchar *row,guchar *outrow, gint x1, gint y1, gint width, gint height, gint channels);

static gboolean blur_dialog (GimpDrawable *drawable);
/* Set up default values for options */
static MyBlurVals avals =
{
  0 /* radius */
};

static MyBlurVals bvals =
{
  0 /* radius */
};


GimpPlugInInfo PLUG_IN_INFO =
{
  NULL,
  NULL,
  query,
  run
};

MAIN();

static void query (void){
  static GimpParamDef args[] =
  {
    {
      GIMP_PDB_INT32,
      "run-mode",
      "Run mode"
    },
    {
      GIMP_PDB_IMAGE,
      "image",
      "Input image"
    },
    {
      GIMP_PDB_DRAWABLE,
      "drawable",
      "Input drawable"
    }
  };

  gimp_install_procedure (
    "plug-in-distortion",
    "Distortion",
    "distortion",
    "Krystian Maciąg",
    "Copyright Krystian Maciąg",
    "2015",
    "Distortion",
    "RGB*, GRAY*",
    GIMP_PLUGIN,
    G_N_ELEMENTS (args), 0,
    args, NULL);

  gimp_plugin_menu_register ("plug-in-distortion",
                             "<Image>/myFilters");
}

static void run (const gchar* name, gint nparams, const GimpParam  *param, gint *nreturn_vals,GimpParam **return_vals){
  static GimpParam  values[1];
  GimpPDBStatusType status = GIMP_PDB_SUCCESS;
  GimpRunMode       run_mode;
  GimpDrawable     *drawable;

  /* Setting mandatory output values */
  *nreturn_vals = 1;
  *return_vals  = values;

  values[0].type = GIMP_PDB_STATUS;
  values[0].data.d_status = status;

  /* Getting run_mode - we won't display a dialog if
   * we are in NONINTERACTIVE mode */
  run_mode = param[0].data.d_int32;

  /*  Get the specified drawable  */
  drawable = gimp_drawable_get (param[2].data.d_drawable);

  switch (run_mode)
  {
  case GIMP_RUN_INTERACTIVE:
    /* Get options last values if needed */
    gimp_get_data ("plug-in-distortion", &avals);
    gimp_get_data ("plug-in-distortion", &bvals);

    /* Display the dialog */
    if (! blur_dialog (drawable))
      return;
    break;

    case GIMP_RUN_NONINTERACTIVE:
    if (nparams != 4)
      status = GIMP_PDB_CALLING_ERROR;
    if (status == GIMP_PDB_SUCCESS)
      bvals.radius = param[3].data.d_int32;
    break;

  case GIMP_RUN_WITH_LAST_VALS:
    /*  Get options last values if needed  */
    gimp_get_data ("plug-in-distortion", &avals);
    gimp_get_data ("plug-in-distortion", &bvals);
    break;

  default:
    break;
  }

  blur (drawable);

  gimp_displays_flush ();
  gimp_drawable_detach (drawable);

  /*  Finally, set options in the core  */
  if (run_mode == GIMP_RUN_INTERACTIVE)
    gimp_set_data ("plug-in-distortion", &avals, sizeof (MyBlurVals));
    gimp_set_data ("plug-in-distortion", &bvals, sizeof (MyBlurVals));
  return;

  //  printf("%f %f \n", avals.radius, bvals.radius);
}

static void blur (GimpDrawable *drawable){
  gint         channels;
  gint         x1, y1, x2, y2;
  GimpPixelRgn rgn_in, rgn_out;
  guchar      *rect = 0;
  guchar      *outrect= 0;
  gint         width, height;

  gimp_progress_init ("My Blur...");

  gimp_drawable_mask_bounds (drawable->drawable_id,
                             &x1, &y1,
                             &x2, &y2);
  width  = x2 - x1;
  height = y2 - y1;

  channels = gimp_drawable_bpp (drawable->drawable_id);

  /* Allocate a big enough tile cache */
  gimp_tile_cache_ntiles (2 * (drawable->width / gimp_tile_width () + 1));

  /* Initialises two PixelRgns, one to read original data,
   * and the other to write output data. That second one will
   * be merged at the end by the call to
   * gimp_drawable_merge_shadow() */
  gimp_pixel_rgn_init (&rgn_in,
                       drawable,
                       x1, y1,
                       width, height,
                       FALSE, FALSE);
  gimp_pixel_rgn_init (&rgn_out,
                       drawable,
                       x1, y1,
                       width, height,
                       TRUE, TRUE);

  /* Allocate memory for input and output tile rows */
  init_mem (&rect, &outrect, width * height * channels);
  assert(rect != NULL);
  gimp_pixel_rgn_get_rect (&rgn_in, rect, x1, y1, width, height);


    process_rect(rect,outrect,x1, y1,width, height, channels);

    gimp_pixel_rgn_set_rect (&rgn_out,
                            outrect,
                            x1,
                            y1,
                            width, height);
  /* We could also put that in a separate function but it's
   * rather simple */

  g_free (rect);
  g_free (outrect);


  /*  Update the modified region */
  gimp_drawable_flush (drawable);
  gimp_drawable_merge_shadow (drawable->drawable_id, TRUE);
  gimp_drawable_update (drawable->drawable_id,
                        x1, y1,
                        width, height);
}

static void init_mem (guchar **row, guchar **outrow, gint num_bytes){
  /* Allocate enough memory for row and outrow */
  *row = g_new (guchar, num_bytes);
  *outrow = g_new (guchar, num_bytes);
}


static double dist(double x, double y){
  //printf("%f \n", (double)x*x + (double)y*y);
  return sqrt((double)x*x + (double)y*y);
}

static double interpolation(double w, double h, guchar A, guchar B, guchar C, guchar D){

  return (double)A *(1.0 - (double)h)* (1.0 -(double) w) + (double)B *((double)w) *(1.0 - (double)h) +
  (double)C * ((double)h) *(1.0 - (double)w) + (double)B * ((double)w *(double)h) ;
}

static double func(double r, double a, double b){
  return 1.0 + r * r * a + r * r * r * r  * b;
}

double lerp(double s, double e, double t){
  return s+(e-s)*t;
  }

double blerp(double c00, double c10, double c01, double c11, double tx, double ty){
    //printf("%f %f %f %f %f %f\n", c00,c10, c01, c11, tx, ty  );
    return lerp(lerp(c00, c10, tx), lerp(c01, c11, tx), ty);
}

static void process_rect (guchar *rect, guchar* outrect, gint x1, gint y1, gint width, gint height, gint channels){
  gint i,j,k;
  //guchar *outtmp = g_new (guchar, width*height*channels);
    printf("%d %d \n", avals.radius, bvals.radius);
    for(i = 0; i < height; i++){
      for (j = 0; j < width; j++){
        for (k = 0; k < channels; k++){
          double centerX = ((double)(width - 1)) / 2.0;
          double centerY = ((double)(height - 1)) / 2.0;

          double deltaX = ((double)j - centerX);

          double deltaY =-1.0 * ((double)i - centerY);

          double dX = deltaX/centerX;

          double dY = deltaY/centerY;


          double r = dist(dX, dY);
          //printf("%f %f %f\n", dX, dY, r);
          //  printf("%f\n", r);
          double nr = func(r, (double)avals.radius/100.0, (double)bvals.radius/100.0);
          //  printf("%f\n", nr);

          double srcXd = (dX *nr) * centerX + centerX;;
          double srcYd = (dY *nr) * (-1.0 * centerY) + centerY ;

          int srcX = (int) srcXd;
          int srcY = (int) srcYd;



           int Ax = CLAMP (srcX, 0, width - 1);
           int Ay = CLAMP (srcY, 0, height - 1);

           int Bx = CLAMP (srcX + 1, 0, width - 1);
           int By = CLAMP (srcY, 0, height - 1);

           int Cx = CLAMP (srcX, 0, width - 1);
           int Cy = CLAMP (srcY + 1, 0, height - 1);

           int Dx = CLAMP (srcX, 0, width - 1);
           int Dy = CLAMP (srcY + 1, 0, height - 1);

        //   printf("%f %f \n", new_value, (guchar) new_value);
          /* double new_value = interpolation(srcXd - (double)floor(srcXd), srcYd - (double)floor(srcYd),
             (double)rect[width * Ay * channels + Ax * (channels-1) + Ax + k],
             (double)rect[width * By * channels + Bx * (channels-1) + Bx + k],
             (double)rect[width * Cy * channels + Cx * (channels-1) + Cx + k],
             (double)rect[width * Dy * channels + Cx * (channels-1) + Dx + k]
           );*/
             double new_value = blerp(
               (double)rect[width * Ay * channels + Ax * (channels-1) + Ax + k],
               (double)rect[width * By * channels + Bx * (channels-1) + Bx + k],
               (double)rect[width * Cy * channels + Cx * (channels-1) + Cx + k],
               (double)rect[width * Dy * channels + Cx * (channels-1) + Dx + k],
               srcXd - (double)floor(srcXd), srcYd - (double)floor(srcYd)
               );

            //  printf("%f %u \n", new_value, (guchar) new_value);
              outrect[width * i * channels +  j * (channels - 1)  + j + k] = (guchar)new_value;

      }
    }
  }
  avals.radius = 0;
  bvals.radius = 0.
  /*  for (k = 0; k < channels*width * height; k++){
    printf("%u \n",rect[k]);
  }*/
}

static gboolean blur_dialog (GimpDrawable * drawable){
  GtkWidget *dialog;
  GtkWidget *main_vbox;
  GtkWidget *main_hbox;
  GtkWidget *frame;
  GtkWidget *radius_label_a;
  GtkWidget *radius_label_b;
  GtkWidget *alignment;
  GtkWidget *spinbutton0;
  GtkObject *spinbutton_adj0;
  GtkWidget *spinbutton1;
  GtkObject *spinbutton_adj1;

  GtkWidget *frame_label;
  gboolean   run;

  gimp_ui_init ("myblur", FALSE);

  dialog = gimp_dialog_new ("Distortion", "Distortion",
                            NULL, 0,
                            gimp_standard_help_func, "plug-in-distortion",
                            GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
                            GTK_STOCK_OK,     GTK_RESPONSE_OK,
                            NULL);

  main_vbox = gtk_vbox_new (FALSE, 6);
  gtk_container_add (GTK_CONTAINER (GTK_DIALOG (dialog)->vbox), main_vbox);
  gtk_widget_show (main_vbox);

  frame = gtk_frame_new (NULL);
  gtk_widget_show (frame);
  gtk_box_pack_start (GTK_BOX (main_vbox), frame, TRUE, TRUE, 0);
  gtk_container_set_border_width (GTK_CONTAINER (frame), 6);

  alignment = gtk_alignment_new (0.5, 0.5, 1, 1);
  gtk_widget_show (alignment);
  gtk_container_add (GTK_CONTAINER (frame), alignment);
  gtk_alignment_set_padding (GTK_ALIGNMENT (alignment), 6, 6, 6, 6);

  main_hbox = gtk_hbox_new (FALSE, 0);
  gtk_widget_show (main_hbox);
  gtk_container_add (GTK_CONTAINER (alignment), main_hbox);

  radius_label_a = gtk_label_new_with_mnemonic ("_A:");


  radius_label_b = gtk_label_new_with_mnemonic ("_B:");

  gtk_box_pack_start (GTK_BOX (main_hbox), radius_label_a, FALSE, FALSE, 6);
  //  gtk_label_set_justify (GTK_LABEL (radius_label_a), GTK_JUSTIFY_RIGHT);

  spinbutton_adj0 = gtk_adjustment_new (0.0, -100.0, 100.0, 1.0, 0.0, 0.0);
  spinbutton0 = gtk_spin_button_new (GTK_ADJUSTMENT (spinbutton_adj0), 1, 0);

  gtk_widget_show (spinbutton0);
  gtk_widget_show (radius_label_a);

  gtk_box_pack_start (GTK_BOX (main_hbox), radius_label_b, FALSE, FALSE, 6);
  //  gtk_label_set_justify (GTK_LABEL (radius_label_b), GTK_JUSTIFY_RIGHT);

  spinbutton_adj1 = gtk_adjustment_new (0.0, -100.0, 100.0, 1.0, 0.0, 0.0);
  spinbutton1 = gtk_spin_button_new (GTK_ADJUSTMENT (spinbutton_adj1), 1.0, 0.0);

  gtk_widget_show (spinbutton1);
  gtk_widget_show (radius_label_b);

  //gtk_label_set_justify (GTK_LABEL (radius_label_c), GTK_JUSTIFY_RIGHT);



  gtk_box_pack_start (GTK_BOX (main_hbox), spinbutton0, FALSE, FALSE, 6);
  gtk_spin_button_set_numeric (GTK_SPIN_BUTTON (spinbutton0), TRUE);

  gtk_box_pack_start (GTK_BOX (main_hbox), spinbutton1, FALSE, FALSE, 6);
  gtk_spin_button_set_numeric (GTK_SPIN_BUTTON (spinbutton1), TRUE);


  frame_label = gtk_label_new ("<b>Parameters</b>");
  gtk_widget_show (frame_label);
  gtk_frame_set_label_widget (GTK_FRAME (frame), frame_label);
  gtk_label_set_use_markup (GTK_LABEL (frame_label), TRUE);

  g_signal_connect (spinbutton_adj0, "value_changed",
                    G_CALLBACK (gimp_int_adjustment_update),
                    &avals.radius);

  g_signal_connect (spinbutton_adj1, "value_changed",
                    G_CALLBACK (gimp_int_adjustment_update),
                    &bvals.radius);



  gtk_widget_show (dialog);

  run = (gimp_dialog_run (GIMP_DIALOG (dialog)) == GTK_RESPONSE_OK);

  gtk_widget_destroy (dialog);

  return run;
}
