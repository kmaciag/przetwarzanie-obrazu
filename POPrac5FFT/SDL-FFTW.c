/*gcc -o sdl-fft sdl-fft.c -Wall -O -I/usr/include/SDL -lSDL_image -lSDL -lfftw

    Fourier transform:  FFTW + SDL         A.Łukaszewski example based on:
    ----------------------------------------------------------------------
    showimage:  A test application for the SDL image loading library.
    Copyright (C) 1997-2009 Sam Lantinga

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    Sam Lantinga
    slouken@libsdl.org
*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <SDL/SDL.h>
#include <SDL/SDL_image.h>


#include <math.h>
#include <fftw.h>

// Dodatkowe funkcje pomocnicze ----------------------------------------
typedef struct {
  double re, im;
} zomplex;


fftwnd_plan    *plan;

void initialise_fft (int width, int height)
{
  plan     = ((fftwnd_plan *) malloc (3 * sizeof (fftwnd_plan))) + 1;
  plan[-1] = fftw2d_create_plan (height, width, (fftw_direction) - 1,
                                 FFTW_ESTIMATE | FFTW_IN_PLACE);
  plan[1]  = fftw2d_create_plan (height, width, (fftw_direction)1,
                                 FFTW_ESTIMATE | FFTW_IN_PLACE);
}

void nosgi_fft (zomplex *array, int width, int height, int inv)
{
  fftw_complex *carray = (fftw_complex *) malloc (width * height * sizeof (fftw_complex));
  int ind;

  for (ind = 0; ind < width * height; ind++) {
    carray[ind].re = array[ind].re;
    carray[ind].im = array[ind].im;
  }

  fftwnd_one (plan[inv], carray, NULL);

  for (ind = 0; ind < width * height; ind++) {
    array[ind].re = carray[ind].re;
    array[ind].im = carray[ind].im;
  }

  free (carray);
}

void compute_fft (zomplex *array, int width, int height)
{
  nosgi_fft (array, width, height, -1);
}

void compute_inverse_fft (zomplex *array, int width, int height)
{
  int ind;
  int value = width * height;

  nosgi_fft (array, width, height, 1);
  for (ind = 0; ind < value; ind++)
    array[ind].re /= value;
}

void gaussian_filter (zomplex *filter, int width, int height, double scale)
{
  static double    k                = 1. / (2. * 1.4142136);
  int    x, y, i;
  double x1, y1, s;
  double a = 1. / (k * scale);
  double c = 1. / 4.;

  for (i = 0, y = 0; y < height; y++) {
    y1 = (y >= height / 2) ? y - height : y;
    s  = erf (a * (y1 - .5)) - erf (a * (y1 + .5));
    for (x = 0; x < width; x++, i++) {
      x1 = (x >= width / 2) ? x - width : x;
      filter[i].re = s * (erf (a * (x1 - .5)) - erf (a * (x1 + .5))) * c;
      filter[i].im = 0.;
    }
  }
}
//------------------------------------------------------------------------

int MIN(int x, int y) { return (x < y ? x : y); }

unsigned char *ImgPtr(int x, int y, SDL_Surface *s) // For 1byte/channel 3-4
{
  return s->pixels + s->format->BytesPerPixel * (x + y * s->w)  ;
}

void test(zomplex* my_fft, int width, int height) {

}

void test1(zomplex* my_fft, int width, int height) {
  for  (int y = 0; y < height; y++)
    for  (int x = 0; x < width; x++) {

      my_fft[x + width * y].im = 0;
    }
}

void test2(zomplex* my_fft, int width, int height) {
  for  (int y = 0; y < height; y++)
    for  (int x = 0; x < width; x++) {
      my_fft[x + width * y].re = 0.0 ;
    }
}
void test3(zomplex* my_fft, int width, int height) {
  for  (int y = 0; y < height; y++)
    for  (int x = 0; x < width; x++) {
      double r = sqrt(my_fft[x + width * y].re * my_fft[x + width * y].re +
                      my_fft[x + width * y].im * my_fft[x + width * y].im);

      my_fft[x + width * y].re = r;
      my_fft[x + width * y].im = 0.0;
    }
}
void test4(zomplex* my_fft, int width, int height) {

  for  (int y = 0; y < height; y++)
    for  (int x = 0; x < width; x++) {
      /* double phi = atan2(my_fft[x + width * y].re, my_fft[x + width * y].im);
       my_fft[x + width * y].re = phi;//(phi - min) / (max - min) * 255.0;
       my_fft[x + width * y].im = 0.0;*/      
      double r = sqrt(my_fft[x + width * y].re * my_fft[x + width * y].re +
                      my_fft[x + width * y].im * my_fft[x + width * y].im);
      my_fft[x + width * y].re /= r;
      my_fft[x + width * y].im /= r;

      my_fft[x + width * y].re *= 3048.0;
      my_fft[x + width * y].im *= 3048.0; 
    }

}
void test5(zomplex* my_fft, int width, int height) {
  for  (int y = 0; y < height; y++)
    for  (int x = 0; x < width; x++) {
      my_fft[x + width * y].im *= -1.0;
    }
}

void test6(zomplex* my_fft, int width, int height) {
  for  (int y = 0; y < height; y++)
    for  (int x = 0; x < width; x++) {
      my_fft[x + width * y].re *= 0.25;
    }
}

void test7(zomplex* my_fft, int width, int height) {
  for  (int y = 0; y < height; y++)
    for  (int x = 0; x < width; x++) {
      my_fft[x + width * y].re = fabs((my_fft[x + width * y].re * (double)(x*x + y*y))/((double)(width + height))); 
      my_fft[x + width * y].im = fabs((my_fft[x + width * y].im * (double)(x*x + y*y))/((double)(width + height))); 
      //printf("%f %f\n", my_fft[x + width * y].re,my_fft[x + width * y].im);
          }
}
void test8(zomplex* my_fft, int width, int height) {
  for  (int y = 0; y < height; y++)
    for  (int x = 0; x < width; x++) {

      if (!(fabs(x) < 25 && fabs(y) < 25 || x < 25 && fabs(height - y) < 25
            || fabs(width - x) < 25 && fabs(height - y) < 25 || fabs(width - x) < 25 && y < 25 )) {
        my_fft[x + width * y ].re = 0.0;
        my_fft[x + width * y].im = 0.0;
      }


    }
}
void test9(zomplex* my_fft, int width, int height) {
  for  (int y = 0; y < height; y++)
    for  (int x = 0; x < width; x++) {
      my_fft[x + width * y].re *= pow(-1.0, (double)(x + y));
      my_fft[x + width * y].im *= pow(-1.0, (double)(x + y));
    }
}



void DoFFTandIFFT(SDL_Surface *img, SDL_Surface *fft, SDL_Surface *rimg, SDL_Surface *aimg, SDL_Surface *bimg, SDL_Surface *phiimg, void (*test)(zomplex*, int, int), int mode)
{
  // ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
  int      width = img->w, height = img->h;
  //int      len=width*height;
  zomplex *my_fft;//, *kernel;
  int      x, y;

  my_fft =  (zomplex*) calloc ((int)width * height, sizeof (zomplex));
  //kernel =  (zomplex*) calloc ((int)width*height, sizeof (zomplex));
  for  (y = 0; y < height; y++)
    for  (x = 0; x < width; x++) {
      double sign = ((x + y) % 2 == 0) ? 1.0 : -1.0;

      if (mode == 1) {
        my_fft[x + width * y].re = (0.3 * ImgPtr(x, y, img)[0] + 0.6 * ImgPtr(x, y, img)[1] + 0.1 * ImgPtr(x, y, img)[2]) * sign;
      } else {
        my_fft[x + width * y].re = 0.3 * ImgPtr(x, y, img)[0] + 0.6 * ImgPtr(x, y, img)[1] + 0.1 * ImgPtr(x, y, img)[2];
      }

      my_fft[x + width * y].im = 0;
    }

  initialise_fft(width, height);

  compute_fft(my_fft, width, height);

  test(my_fft, width, height);


  double maxphi =  -width * height;
  double minphi =  width * height;


  for  (y = 0; y < height; y++)
    for  (x = 0; x < width; x++) {
      double phi = atan2(my_fft[x + width * y].re, my_fft[x + width * y].im);

      if (minphi > phi) {
        minphi = phi;
      }
      if (maxphi < phi) {
        maxphi = phi;
      }


    }


  for  (y = 0; y < height; y++)
    for  (x = 0; x < width; x++) {

      double r = sqrt(my_fft[x + width * y].re * my_fft[x + width * y].re +
                      my_fft[x + width * y].im * my_fft[x + width * y].im);

      double phi = atan2(my_fft[x + width * y].re, my_fft[x + width * y].im);

      ImgPtr(x, y, fft)[0] = ImgPtr(x, y, fft)[1] = ImgPtr(x, y, fft)[2] =  MIN( 255, (int)(1024.*log ( 1 + r / (width * height) )) );

      ImgPtr(x,  y , aimg)[0] = ImgPtr(x , y , aimg)[1] = ImgPtr(x , y , aimg)[2] =  MIN( 255, (int)(1024.*log ( 1.0 + fabs(my_fft[x + width * y].re) / (double)(width * height) )) );
      //  printf("%d \n",(int)(50000.*log ( 1 + my_fft[x + width * y].re/(width*height) )));

      ImgPtr(x,  y , bimg)[0] = ImgPtr(x , y , bimg)[1] = ImgPtr(x , y , bimg)[2] =  MIN( 255, (int)(1024.*log ( 1.0 + fabs(my_fft[x + width * y].im) / (double)(width * height) )) );

      ImgPtr(x, y, phiimg)[0] = ImgPtr(x, y, phiimg)[1] = ImgPtr(x, y, phiimg)[2] = (phi - minphi) / (maxphi - minphi) * 255.0;

    }

  compute_inverse_fft(my_fft, width, height);

  y = 0;
  x = 0;
  double max =  -width * height;
  double min = width * height;

  for  (int y = 0; y < height; y++)
    for  (int x = 0; x < width; x++) {
      double r = sqrt(my_fft[x + width * y].re * my_fft[x + width * y].re +
                      my_fft[x + width * y].im * my_fft[x + width * y].im);
      if (min > r) {
        min = r;
      }
      if (max < r) {
        max = r;
      }
    }
 // printf("%f %f\n", max, min);

  for  (y = 0; y < height; y++)
    for  (x = 0; x < width; x++) {
      double r = sqrt(my_fft[x + width * y].re * my_fft[x + width * y].re +
                      my_fft[x + width * y].im * my_fft[x + width * y].im);
      //   printf("%f\n",r);
      ImgPtr(x, y, rimg)[0] = ImgPtr(x, y, rimg)[1] = ImgPtr(x, y, rimg)[2] = r;
      //MIN( 255, (int)(1024.*log ( 1 + r / (width * height) )) );
    }
  free(my_fft);
}

//------------------------------------------------------------------------

/* Draw a Gimpish background pattern to show transparency in the image */


int main(int argc, char *argv[])
{
  Uint32 flags;
  SDL_Surface *screen, *image, *imfft, *rimg, *aimg, *bimg, *phiimg;
  int i, depth, done;
  SDL_Event event;
#if 0
  SDL_RWops* rw_ops;
#endif

  /* Check command line usage */
  if ( ! argv[1] ) {
    fprintf(stderr, "Usage: %s <image_file>\n", argv[0]);
    return (1);
  }

  /* Initialize the SDL library */
  if ( SDL_Init(SDL_INIT_VIDEO) < 0 ) {
    fprintf(stderr, "Couldn't initialize SDL: %s\n", SDL_GetError());
    return (255);
  }

  flags = SDL_SWSURFACE;
  for ( i = 1; argv[i]; ++i ) {
    if ( strcmp(argv[i], "-fullscreen") == 0 ) {
      SDL_ShowCursor(0);
      flags |= SDL_FULLSCREEN;
      continue;
    }

    /* Open the image file */
    image = IMG_Load(argv[i]);
    imfft = IMG_Load(argv[i]);  // Only to alloc for FT
    rimg = IMG_Load(argv[i]);
    aimg = IMG_Load(argv[i]);
    bimg = IMG_Load(argv[i]);
    phiimg = IMG_Load(argv[i]);

    if ( image == NULL ) {
      fprintf(stderr, "Couldn't load %s: %s\n",
              argv[i], SDL_GetError());
      continue;
    }


    /* Create a display for the image */
    depth = SDL_VideoModeOK(image->w, image->h, 32, flags);
    /* Use the deepest native mode, except that we emulate 32bpp
       for viewing non-indexed images on 8bpp screens */
    if ( depth == 0 ) {
      if ( image->format->BytesPerPixel > 1 ) {
        depth = 32;
      } else {
        depth = 8;
      }
    } else if ( (image->format->BytesPerPixel > 1) && (depth == 8) ) {
      depth = 32;
    }
    if (depth == 8)
      flags |= SDL_HWPALETTE;
    screen = SDL_SetVideoMode(3 * image->w, 2 * image->h, depth, flags); //anl!
    if ( screen == NULL ) {
      fprintf(stderr, "Couldn't set %dx%dx%d video mode: %s\n",
              image->w, image->h, depth, SDL_GetError());
      continue;
    }

    /* Set the palette, if one exists */
    if ( image->format->palette ) {
      SDL_SetColors(screen, image->format->palette->colors,
                    0, image->format->palette->ncolors);
    }



    /* DO FFT */
    SDL_Rect LeftR;
    SDL_Rect Right;
    SDL_Rect Inv;
    SDL_Rect a;
    SDL_Rect b;
    SDL_Rect phi;

    done = 0;
    int test_num = 11;

    int mode = 0;

    while ( ! done ) {
      if ( SDL_PollEvent(&event) ) {
        switch (event.type) {
        case SDL_KEYUP:
          switch (event.key.keysym.sym) {
          case SDLK_1:
            test_num = 1;

            break;
          case SDLK_2:
            test_num = 2;

            break;
          case SDLK_3:
            test_num = 3;
            break;
          case SDLK_4:
            test_num = 4;
            break;
          case SDLK_5:
            test_num = 5;
            break;
          case SDLK_6:
            test_num = 6;
            break;
          case SDLK_7:
            test_num = 7;
            break;
          case SDLK_8:
            test_num = 8;
            break;
          case SDLK_9:
            test_num = 9;
            break;
          case SDLK_d:
            test_num = 11;
            break;
          case SDLK_s:
            mode = (mode  + 1) % 2;
            break;
          case SDLK_ESCAPE:
          case SDLK_q:
            argv[i + 1] = NULL;
          default:
            break;
          }
          break;
        case SDL_MOUSEBUTTONDOWN:
          done = 1;
          break;
        case SDL_QUIT:
          argv[i + 1] = NULL;
          done = 1;
          break;
        default:
          break;
        }
      }

      switch (test_num ) {
      case 1:
        DoFFTandIFFT(image, imfft, rimg, aimg, bimg, phiimg, test1, mode);
        SDL_WM_SetCaption("Test 1 ", "showimage");
        break;
      case 2:
        DoFFTandIFFT(image,  imfft, rimg, aimg, bimg, phiimg, test2, mode);
        SDL_WM_SetCaption("Test 2 ", "showimage");
        break;
      case 3:
        DoFFTandIFFT(image,  imfft, rimg, aimg, bimg, phiimg, test3, mode);
        SDL_WM_SetCaption("Test 3", "showimage");
        break;
      case 4:
        DoFFTandIFFT(image,  imfft, rimg, aimg, bimg, phiimg, test4, mode);
        SDL_WM_SetCaption("Test 4", "showimage");
        break;
      case 5:
        DoFFTandIFFT(image,  imfft, rimg, aimg, bimg, phiimg, test5, mode);
        SDL_WM_SetCaption("Test 5", "showimage");
        break;
      case 6:
        DoFFTandIFFT(image,  imfft, rimg, aimg, bimg, phiimg, test6, mode);
        SDL_WM_SetCaption("Test 6", "showimage");
        break;
      case 7:
        DoFFTandIFFT(image,  imfft, rimg, aimg, bimg, phiimg, test7, mode);
        SDL_WM_SetCaption("Test 7", "showimage");
        break;
      case 8:
        DoFFTandIFFT(image,  imfft, rimg, aimg, bimg, phiimg, test8, mode);
        SDL_WM_SetCaption("Test 8", "showimage");
        break;
      case 9:
        DoFFTandIFFT(image,  imfft, rimg, aimg, bimg, phiimg, test9, mode);
        SDL_WM_SetCaption("Test 9", "showimage");
        break;
      case 11:
        DoFFTandIFFT(image,  imfft, rimg, aimg, bimg, phiimg, test, mode);
        SDL_WM_SetCaption("Bezzmian", "showimage");
        break;
      default:
        break;

      }
      LeftR.x = 0;
      LeftR.y = 0;

      Right.x = image->w;
      Right.y = 0;

      a.x = 2 * image->w;
      a.y = 0;

      Inv.x = 0;
      Inv.y = image->h;

      b.x = image->w;
      b.y = image->h;

      phi.x = 2 * image->w;
      phi.y = image->h;

      SDL_BlitSurface(image, NULL, screen, &LeftR);  //anl!
      SDL_BlitSurface(imfft, NULL, screen, &Right);  //anl!
      SDL_BlitSurface(rimg, NULL, screen, &Inv);
      SDL_BlitSurface(aimg, NULL, screen, &a);
      SDL_BlitSurface(bimg, NULL, screen, &b);
      SDL_BlitSurface(phiimg, NULL, screen, &phi);

      SDL_UpdateRect(screen, 0, 0, 0, 0);
    }
    SDL_FreeSurface(image);
    SDL_FreeSurface(imfft);
    SDL_FreeSurface(rimg);
  }

  /* We're done! */
  SDL_Quit();
  return (0);
}
