#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include <math.h>
#include <fftw3.h>
#include <zlib.h>
#include <jpeglib.h>
#include <vector>



fftw_plan    planR, planG, planB;
fftw_plan    inv_planR, inv_planG, inv_planB;
fftw_complex *inR, *inG, *inB, *outR, *outG, *outB, *new_outR, *new_outG, *new_outB;

void initialise_fft (int width, int height)
{
  planR = fftw_plan_dft_2d(height, width, inR, outR, FFTW_FORWARD, FFTW_ESTIMATE);
  planG = fftw_plan_dft_2d(height, width, inG, outG, FFTW_FORWARD, FFTW_ESTIMATE);
  planB = fftw_plan_dft_2d(height, width, inB, outB, FFTW_FORWARD, FFTW_ESTIMATE);
  inv_planR = fftw_plan_dft_2d(height, width, outR, new_outR, FFTW_BACKWARD, FFTW_ESTIMATE);
  inv_planG = fftw_plan_dft_2d(height, width, outG, new_outG, FFTW_BACKWARD, FFTW_ESTIMATE);
  inv_planB = fftw_plan_dft_2d(height, width, outB, new_outB, FFTW_BACKWARD, FFTW_ESTIMATE);

}

void compute_fft ()
{
  fftw_execute(planR);
  fftw_execute(planG);
  fftw_execute(planB);
}

void compute_inverse_fft ()
{
  fftw_execute(inv_planR);
  fftw_execute(inv_planG);
  fftw_execute(inv_planB);
}

void close_fft() {
  fftw_destroy_plan(planR);
  fftw_destroy_plan(inv_planR);
  fftw_destroy_plan(planG);
  fftw_destroy_plan(inv_planG);
  fftw_destroy_plan(planB);
  fftw_destroy_plan(inv_planB);
}


void gaussian_filter (int width, int height, double scale)
{
  /* static double k = 1. / (2. * 1.4142136);
   int    x, y, i;
   double x1, y1, s;
   double a = 1. / (k * scale);
   double c = 1. / 4.;

   for (i = 0, y = 0; y < height; y++) {
     y1 = (y >= height / 2) ? y - height : y;
     s  = erf (a * (y1 - .5)) - erf (a * (y1 + .5));
     for (x = 0; x < width; x++, i++) {
       x1 = (x >= width / 2) ? x - width : x;
       filter[i].re = s * (erf (a * (x1 - .5)) - erf (a * (x1 + .5))) * c;
       filter[i].im = 0.;
     }
   }*/
}


int MIN(int x, int y) { return (x < y ? x : y); }

unsigned char *ImgPtr(int x, int y, SDL_Surface *s) // For 1byte/channel 3-4
{
  return (unsigned char *)(s->pixels + s->format->BytesPerPixel * (x + y * s->w))  ;
}

void DoFFTandIFFT(SDL_Surface *img, SDL_Surface *fft)
{
  /*
    int      width = img->w, height = img->h;
    //int      len=width*height;
    zomplex *my_fft;//, *kernel;
    int      x, y;

    my_fft =  (zomplex*) calloc ((int)width * height, sizeof (zomplex));
    //kernel =  (zomplex*) calloc ((int)width*height, sizeof (zomplex));
    for  (y = 0; y < height; y++)
      for  (x = 0; x < width; x++) {
        double sign = ((x + y) % 2 == 0) ? 1.0 : -1.0;

        if (mode == 1) {
          my_fft[x + width * y].re = (0.3 * ImgPtr(x, y, img)[0] + 0.6 * ImgPtr(x, y, img)[1] + 0.1 * ImgPtr(x, y, img)[2]) * sign;
        } else {
          my_fft[x + width * y].re = 0.3 * ImgPtr(x, y, img)[0] + 0.6 * ImgPtr(x, y, img)[1] + 0.1 * ImgPtr(x, y, img)[2];
        }

        my_fft[x + width * y].im = 0;
      }

    initialise_fft(width, height);
    compute_fft(my_fft, width, height);
    free(my_fft);*/
}


int main(int argc, char *argv[])
{
  Uint32 flags;
  SDL_Surface *screen, *image;
  int i, depth, done;
  SDL_Event event;
     SDL_Init(SDL_INIT_EVERYTHING);
#if 0
  SDL_RWops* rw_ops;
#endif

  if ( ! argv[1] ) {
    fprintf(stderr, "Usage: %s <image_file>\n", argv[0]);
    return (1);
  }
  printf("%s \n", argv[1]);
  if ((IMG_Init(IMG_INIT_JPG) & IMG_INIT_JPG) != IMG_INIT_JPG) {
    fprintf(stderr, "Couldn't initialize SDL: %s\n", SDL_GetError());
    SDL_Quit();
    return 1;
  }
  if ( SDL_Init(SDL_INIT_VIDEO) < 0 ) {
    fprintf(stderr, "Couldn't initialize SDL: %s\n", SDL_GetError());
    return (255);
  }
  image = IMG_Load(argv[i]);

  if ( image == NULL ) {
    fprintf(stderr, "Couldn't load %s: %s\n",
            argv[i], SDL_GetError());
  }

  SDL_Rect LeftR;
  SDL_Rect Right;

  done = 0;
  int test_num = 11;

  int mode = 0;

  while ( ! done ) {
    if ( SDL_PollEvent(&event) ) {
      switch (event.type) {
      case SDL_KEYUP:
        switch (event.key.keysym.sym) {
        case SDLK_1:
          test_num = 1;
          break;
        case SDLK_ESCAPE:
        case SDLK_q:
          argv[i + 1] = NULL;
        default:
          break;
        }
        break;
      case SDL_MOUSEBUTTONDOWN:
        done = 1;
        break;
      case SDL_QUIT:
        argv[i + 1] = NULL;
        done = 1;
        break;
      default:
        break;
      }
    }
    
    LeftR.x = 0;
    LeftR.y = 0;

    Right.x = image->w;
    Right.y = 0;

    SDL_BlitSurface(image, NULL, screen, &LeftR);  //anl!

  }
  SDL_FreeSurface(image);


  SDL_Quit();
  return (0);
}
